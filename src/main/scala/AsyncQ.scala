import java.util.concurrent.atomic.{AtomicReference, AtomicInteger}
import scala.collection.mutable

/**
 * Questions:
 * How to calculate thread priorities?? what is relative thread order? relative in time?
 *
 * Spec:
 * maintain WorkerCount running threads for work, SchedulerCount threads for scheduler
 * threads are (extended) java.lang.Thread
 * Threads are suspend by itself and are awakened by Schedulers
 *
 * Note: C/C++ implementation may use pthreads or (pre POSIX.1-2008) (get,set,make,swap)context
 *  to suspend and resume work threads
 */
class AsyncQ(val WorkerCount: Int, val SchedulerCount: Int) {
  val Qmax: Int = WorkerCount + SchedulerCount
  var runningThreads = new AtomicInteger()
  @volatile var memoryLimitPerThread = 0

  // Queue of suspended threads to be scheduled
  val Qin = new mutable.SynchronizedQueue[SuspendEvent]
  // Queue of ready to run threads
  val Qout = new mutable.SynchronizedQueue[MyThread]

  class SyncVar[T] {
    private val value: AtomicReference[Either[T, List[MyThread]]] = new AtomicReference(Right(Nil))
    def isWritten = value.get().isLeft
    /** atomic */
    def writeAndGetWaitingList(v: T) = value.getAndSet(Left(v)).right.get
    /** atomic
     *  @return Some(value) if it was written
      *         None if not yet written. and <b>update waiting list</b> */
    def attemptRead(t: MyThread): Option[T] = {
      var res: Option[T] = None
      var done = false
      do {
        value.get() match {
          case Left(v) => {
            done = true
            res = Some(v)
          }
          case oldv@Right(lst) => {
            // in paper: scheduler should update waiting list
            val newv = Right(t :: lst)
            if (value.compareAndSet(oldv, newv)) {
              done = true
              res = None
            }
          }
        }
      } while (!done)
      res
    }
    def read() = value.get.left.get
  }

  abstract class SuspendEvent()
  /** @param waitList - list of thread waiting for read a var */
  case class WriteSyncVar[T](thread: MyThread, waitList: List[MyThread]) extends SuspendEvent
  case class Terminated(thread: MyThread) extends SuspendEvent
  case class Fork(thread: MyThread, newThread: MyThread) extends SuspendEvent
  case class ReadSyncVar[T](thread: MyThread, syncVar: SyncVar[T]) extends SuspendEvent
  case class MemoryLimit(thread: MyThread) extends SuspendEvent

  val curPriority = new AtomicInteger()

  type ThreadFunc = (MyThread => Unit)

  // One MyThread instance per work thread
  class MyThread(val f: ThreadFunc) extends Thread with Ordered[MyThread] {
    val myPriority: Int = curPriority.getAndIncrement
    @volatile var isLive = true

    private def suspend(ev: SuspendEvent) = {
      isLive = false
      Qin += ev;
      this.synchronized {
        while (!isLive) // for spurious wakeups
          this.wait();
      }
    }

    def readSyncVar[T](v: SyncVar[T]) = {
      v.attemptRead(this).getOrElse {
        suspend(ReadSyncVar(this, v))
        v.read
      }
    }

    def fork(f: ThreadFunc) = suspend(Fork(this, new MyThread(f)))

    def writeSyncVar[T](variable: SyncVar[T], value: T) = {
      val lst = variable.writeAndGetWaitingList(value)
      if (lst != Nil)
        suspend(WriteSyncVar(this, lst))
    }

    var memoryUsage = 0 // per task
    def checkMemoryLimit(d: Int) = {
      if (memoryUsage + d > memoryLimitPerThread) {
        memoryUsage = 0 // new task -> new limit
        suspend(MemoryLimit(this))
      }
    }
    def alloc(n: Int): Array[Int] = {
      checkMemoryLimit(n)
      memoryUsage += n
      return new Array[Int](n)
    }

    def allocSyncVar[T]() = {
      checkMemoryLimit(1)
      memoryUsage += 1
      new SyncVar[T]
    }

    def free(arr: Array[Int]) = memoryUsage -= arr.size
    def free[T](v: SyncVar[T]) = memoryUsage -= 1

    override def run(): Unit = {
      f(this)
      Qin += Terminated(this)
    }

    def compare(t: MyThread) = myPriority - t.myPriority
  }

  trait Scheduler extends Runnable {
    // priority queue of live (ready but not scheduled & suspended) threads
    val liveThreads: BWPQ[MyThread]
    def isThreadsInSystem = runningThreads.get()>0 || !Qin.isEmpty || !Qout.isEmpty || !liveThreads.isEmpty
    def calcMemoryLimitPerTask = Math.ceil(Math.log(liveThreads.count + WorkerCount + SchedulerCount)).toInt
  }

  // single-threaded scheduler
  val SingleScheduler = new Scheduler {
    val liveThreads = new SingleThreadedBWPQ[MyThread]()
    override def run() {
      println("SchedulerThread1: start")
      while (isThreadsInSystem) {
        val qin = Qin.dequeueAll(_ => true)
        for (event <- qin) {
          event match {
            case WriteSyncVar(t, waitList) => {
              liveThreads.markReady(waitList)
              liveThreads.markReady(List(t))
            }
            case Terminated(t) =>
              liveThreads.remove(t)
            case Fork(t, nt) => {
              liveThreads.fork(t, nt)
            }
            case ReadSyncVar(t, v) =>
              liveThreads.markSuspended(t)
            case MemoryLimit(t) =>
              liveThreads.markReady(List(t))
          }
          runningThreads.decrementAndGet()
        }
        Qout ++= liveThreads.listReady(Qmax - Qout.size)
        memoryLimitPerThread = calcMemoryLimitPerTask
        // in paper: workers should get awaken threads from Qout and execute them
        while (runningThreads.get() < WorkerCount && !Qout.isEmpty) {
          val t = Qout.dequeue()
          if (t.isLive)
            t.start() // start fork // TODO: suspend in MyThread#run
          else {
            t.synchronized {
              t.isLive = true
              t.notify()
            }
          }
          runningThreads.incrementAndGet()
        }
      }
      println("SchedulerThread1: stop")
      this.synchronized { this.notify(); }
    }
  }

  /** TODO: multithreaded scheduler */
  def MultiSchedulerThread: Runnable = ???

  def run(mainFunc: ThreadFunc) {
    runningThreads.incrementAndGet()
    val mainThread = new MyThread(mainFunc)
    mainThread.start()
    if (SchedulerCount == 1)
      new Thread(SingleScheduler).start()
    else
      new Thread(MultiSchedulerThread).start()
    // exit runner main thread
    SingleScheduler.synchronized {
      while (SingleScheduler.isThreadsInSystem)
        SingleScheduler.wait();
    }
  }
}