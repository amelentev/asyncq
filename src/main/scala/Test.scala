object Test extends App {
  val scheduler = new AsyncQ(8, 1) {
    val mainThread: ThreadFunc = { main =>
      println("main: start")
      val v1: SyncVar[Int] = main.allocSyncVar()
      val v2: SyncVar[Int] = main.allocSyncVar()
      main.fork { t1 =>
        println("fork: start")
        val val1 = t1.readSyncVar(v1)
        println(s"fork: Got from main: $val1")
        println("fork: write v2")
        t1.writeSyncVar(v2, 1+val1)
        println("fork: stop")
      }
      println("main: write v1")
      main.writeSyncVar(v1, 2)
      val val2 = main.readSyncVar(v2)
      println(s"main: Got from fork: $val2")
      println("main: stop")
    }
  }
  scheduler.run(scheduler.mainThread)
}