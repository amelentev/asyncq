import scala.collection.mutable

trait BWPQ[A] {
  def isEmpty: Boolean
  def count: Int
  def markReady(lst: Seq[A])
  def markSuspended(e: A)
  def remove(e: A)
  def fork(e: A, ne: A)
  def listReady(count: Int): Seq[A]
}

class SingleThreadedBWPQ[A](implicit val ord: Ordering[A]) extends BWPQ[A] {
  val black = new mutable.PriorityQueue[A]() // ready threads
  val white = new mutable.TreeSet[A]() // suspended threads. no running threads here

  override def isEmpty = black.isEmpty && white.isEmpty
  override def count = black.size + white.size

  override def listReady(count: Int) = {
    var res: List[A] = Nil
    while (res.size < count && !black.isEmpty) {
      val t = black.dequeue()
      res = t :: res
      white += t
    }
    res
  }

  override def fork(e: A, ne: A) {
    black.enqueue(ne)
    black.enqueue(e)
  }

  override def remove(e: A) = white -= e

  override def markSuspended(e: A) = white.add(e)

  override def markReady(lst: Seq[A]) = {
    for (a <- lst) {
      black.enqueue(a)
      white.remove(a)
    }
  }
}

/** TODO: multithreaded BWPQ. OpenMP-style bulk operations */
abstract class MultiThreadedBWPQ[A] extends BWPQ[A] {
}

trait P23Tree[A] {
  trait Node
  case class InternalNode(count: Int, c1: Node, c2: Node, c3: Node, parent: InternalNode)
  case class Leaf(data: A, parent: InternalNode)
}