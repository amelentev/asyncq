# Async-Q Scala #

This is an implementation of Async-Q scheduling algorithm from research paper

["Space-Efficient Scheduling of Parallelism with Synchronization Variables"](http://www.cs.cmu.edu/~scandal/papers/synch-spaa97.html)

in Scala language.